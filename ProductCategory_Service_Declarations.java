package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.ProductCategory;

public interface ProductCategory_Service_Declarations 
{

	//display
	public List<ProductCategory>displayAll();
	
	//display Based on ID
	public ProductCategory displayBasedOnId(int pcId);
	
	//insert 
	public void insert(ProductCategory pc);
	
	//update 
	public void update(ProductCategory pc);
	
	//delete
	public void deleteById(int pcId);
	
	
}

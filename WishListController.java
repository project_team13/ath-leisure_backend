package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.WishList;
import com.example.demo.service.WishList_Service_Declarations;

@RestController
@RequestMapping("/wishlist")
public class WishListController {
	
	private WishList_Service_Declarations wishListServ;

	@Autowired
	public WishListController(WishList_Service_Declarations wishListServ) {
		super();
		this.wishListServ = wishListServ;
	}
	@GetMapping("/list")
	public List<WishList> display(){
		return wishListServ.display();
	}
	//delete
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable int id) {
		wishListServ.delete(id);
	}
	
	
}

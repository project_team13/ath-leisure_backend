package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ProductCategory")
public class ProductCategory{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int pcId;
	@Column(name="Name")
	private String pcName;
	@Column(name="Gender")
	private String pcGender;
	
	public ProductCategory() 
	{
		
	}

	public ProductCategory(int pcId, String pcName, String pcGender) {
		super();
		this.pcId = pcId;
		this.pcName = pcName;
		this.pcGender = pcGender;
	}

	public int getPcId() {
		return pcId;
	}

	public void setPcId(int pcId) {
		this.pcId = pcId;
	}

	public String getPcName() {
		return pcName;
	}

	public void setPcName(String pcName) {
		this.pcName = pcName;
	}

	public String getPcGender() {
		return pcGender;
	}

	public void setPcGender(String pcGender) {
		this.pcGender = pcGender;
	}

	@Override
	public String toString() {
		return "ProductCategory [pcId=" + pcId + ", pcName=" + pcName + ", pcGender=" + pcGender + "]";
	}
	
	
}

package com.example.demo.persistance;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Product;

public interface Product_Repository extends JpaRepository<Product, Integer> {

}

package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Product;
import com.example.demo.persistance.Product_Repository;

@Service
public class Product_Service_Implementations implements Product_Service_Declarations {

	private Product_Repository prep;
	
	@Autowired
	public Product_Service_Implementations(Product_Repository prep) {
		super();
		this.prep = prep;
	}

	
	@Override
	@Transactional
	public List<Product> displayAllProducts() {
		
		return prep.findAll();
	}


	@Override
	@Transactional
	public Product displayProductById(int id) {
		return prep.findById(id).get();
	}

	@Override
	@Transactional
	public void insertProduct(Product p) {
		prep.save(p);
	}

	@Override
	@Transactional
	public void updateProduct(Product p) {
		prep.save(p);
	}

	@Override
	@Transactional
	public void deleteProduct(int id) {
		prep.deleteById(id);
		
	}

}

package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Product")
public class Product {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="pId")
	private int pId;
	
	@Column(name="pName")
	private String pName;
	
	@Column(name="price")
	private double price;
	
	@Column(name="Description")
	private String description;
	
	@Column(name="stock")
	private int stock;
	
	@Column(name="size")
	private String size;
	
	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public Product(int stock, String size) {
		super();
		this.stock = stock;
		this.size = size;
	}

	@Column(name="pCid")
	private Integer pCid;
	
	public Product() {
		
	}

	public Product(int pId, String pName, double price, String description, int stock, String size, Integer pCid) {
		super();
		this.pId = pId;
		this.pName = pName;
		this.price = price;
		this.description = description;
		this.stock = stock;
		this.size = size;
		this.pCid = pCid;
	}

	public int getpId() {
		return pId;
	}

	public void setpId(int pId) {
		this.pId = pId;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getpCid() {
		return pCid;
	}

	public void setpCid(Integer pCid) {
		this.pCid = pCid;
	}

	@Override
	public String toString() {
		return "Product [pId=" + pId + ", pName=" + pName + ", price=" + price + ", description=" + description
				+ ", stock=" + stock + ", size=" + size + ", pCid=" + pCid + "]";
	}

	
	
	
}

package com.example.demo.persistance;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.ProductCategory;

public interface ProductCategory_Repository extends JpaRepository<ProductCategory, Integer> 
{

}

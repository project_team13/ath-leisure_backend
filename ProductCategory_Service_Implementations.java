package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.ProductCategory;
import com.example.demo.persistance.ProductCategory_Repository;

@Service
public class ProductCategory_Service_Implementations implements ProductCategory_Service_Declarations
{
	private ProductCategory_Repository pcrep;
	@Autowired
	public ProductCategory_Service_Implementations(ProductCategory_Repository pcrep) 
	{
		this.pcrep = pcrep;
	}

	@Override
	@Transactional
	public List<ProductCategory> displayAll() 
	{
		return pcrep.findAll();
	}

	@Override
	@Transactional
	public ProductCategory displayBasedOnId(int pcId) 
	{
		return pcrep.findById(pcId).get();
	}

	@Override
	@Transactional
	public void insert(ProductCategory pc) 
	{
		pcrep.save(pc);
	}

	@Override
	@Transactional
	public void update(ProductCategory pc) 
	{
		pcrep.save(pc);
	}

	@Override
	@Transactional
	public void deleteById(int pcId) 
	{
		pcrep.deleteById(pcId);
	}

}
